class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :first_name
      t.string :last_name
      t.integer :unit_id
      t.integer :type_id
      t.string :street_address_1
      t.string :street_address_2
      t.string :city
      t.integer :state_id
      t.string :zipcode
      t.string :title
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
