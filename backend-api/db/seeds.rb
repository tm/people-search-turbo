# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

u = Person.create({ first_name: 'Han', last_name: 'Solo', unit_id: 1 })
u = Person.create({ first_name: 'Darth', last_name: 'Vader', unit_id: 2 })
u = Person.create({ first_name: 'C', last_name: '3P0', unit_id: 3 })
u = Person.create({ first_name: 'R2', last_name: 'D2', unit_id: 4 })
u = Person.create({ first_name: 'Chew', last_name: 'Baca', unit_id: 5 })

p = Unit.create({ name: 'dept. of awesome' })
