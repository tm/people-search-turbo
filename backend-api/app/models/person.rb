class Person < ActiveRecord::Base
	validates :first_name, :last_name, :title, :email, presence: true
	belongs_to :unit
end
