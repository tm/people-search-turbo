json.extract! @person, :id, :first_name, :last_name, :unit_id, :type_id, :street_address_1, :street_address_2, :city, :state_id, :zipcode, :title, :phone, :email, :created_at, :updated_at
