json.array!(@people) do |person|
  json.extract! person, :id, :first_name, :last_name, :unit_id, :type_id, :street_address_1, :street_address_2, :city, :state_id, :zipcode, :title, :phone, :email
  json.url person_url(person, format: :json)
end
